require 'test_helper'

class ReferralExtractorTest < ActionDispatch::IntegrationTest
  
  test 'return users chains with out calculating points if valid file' do
    file = fixture_file_upload(Rails.root.join('test/fixtures/sample.txt'), 'text/plain')
    events = Rewards::DataValidation.new(file).extract_valid_events
    users_chains = Rewards::ReferralExtractor.new(events).build_referral_chains
    assert_equal users_chains.length, 4
    assert_equal users_chains.values.first.user_id, "A"
    assert_equal users_chains.values.first.points, 0
  end

end