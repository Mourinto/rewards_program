require 'test_helper'

class DataValidationTest < ActionDispatch::IntegrationTest

  test 'return events hashes if valid file' do
    file = fixture_file_upload('test/fixtures/sample.txt', 'text/plain')
    events = Rewards::DataValidation.new(file).extract_valid_events
    assert_equal events.length, 7
    assert_equal events.first[:event_doer], "A"
    assert events.first[:recommends_event], true
  end

end

