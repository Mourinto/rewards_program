require 'test_helper'

class PointsCalculationsTest < ActionDispatch::IntegrationTest

    test 'calculate points for users chains' do
      file = fixture_file_upload(Rails.root.join('test/fixtures/sample.txt'), 'text/plain')
      events = Rewards::DataValidation.new(file).extract_valid_events
      users_chains = Rewards::ReferralExtractor.new(events).build_referral_chains
      results = Rewards::PointsCalculations.new(users_chains).calculate_points
      assert_equal users_chains.length, 4
      assert_equal users_chains.values.first.user_id, "A"
      assert_equal results["A"].points, 1.75
      assert_equal results["B"].points, 1.5
      assert_equal results["C"].points, 1
      assert_equal results["D"].points, 0
  end
end