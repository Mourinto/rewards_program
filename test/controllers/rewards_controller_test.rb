require 'test_helper'

class RewardsControllerTest < ActionDispatch::IntegrationTest

  test 'return users ids and points for valid file' do
    file = fixture_file_upload(Rails.root.join('test/fixtures/sample.txt'), 'text/plain')
    post '/calculate_rewards', params: { file: file }, headers: { 'content-type': 'text/plain' }
    result = JSON.parse(response.body)
    assert_equal response.status, 200
    assert_equal result["A"], 1.75
    assert_equal result["B"], 1.5
    assert_equal result["C"], 1.0
    assert_equal result["D"], 0
  end

  test 'raise Error for invalid file' do
    file = fixture_file_upload(Rails.root.join('test/fixtures/invalid_sample.txt'), 'text/plain')
    post '/calculate_rewards', params: { file: file }, headers: { 'content-type': 'text/plain' }
    result = JSON.parse(response.body)
    assert_equal result["error"], 'Data is corrupted!, please provide valid data.'
  end

  test 'raise Error for empty file' do
    file = fixture_file_upload(Rails.root.join('test/fixtures/empty_sample.txt'), 'text/plain')
    post '/calculate_rewards', params: { file: file }, headers: { 'content-type': 'text/plain' }
    result = JSON.parse(response.body)
    assert_equal result["error"], 'File is Empty!, please provide a valid file.'
  end

  test 'raise Error for no file' do
    #file = fixture_file_upload(Rails.root.join('test/fixtures/empty_sample.txt'), 'text/plain')
    post '/calculate_rewards' #, params: { file: file }, headers: { 'content-type': 'text/plain' }
    result = JSON.parse(response.body)
    assert_equal result["error"],'Please Attach Valid File'
  end

  test 'raise Error for wrong content type file' do
    file = fixture_file_upload(Rails.root.join('test/fixtures/logo.png'), 'image/png')
    post '/calculate_rewards' , params: { file: file }, headers: { 'content-type': 'image/png' }
    result = JSON.parse(response.body)
    assert_equal result["error"],'Invalid File Type, Please attach text file!'
  end

end