ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require "rack/test"
require "minitest/autorun"
require "minitest/spec"

class ActiveSupport::TestCase
  parallelize(workers: :number_of_processors)
  fixtures :all
end
