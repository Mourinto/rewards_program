# Rewards Program

* Ruby version: 2.6.0
* Rails version: 6.0.0
* How to Run the project

navigate to project dir, then in terminal type `bundle install`
start the server `rails s`
then from the terminal 
```curl -F 'file=@path_to_sample.txt' http://localhost:3000/calculate_rewards```

to run console just open the app in terminal then  type `rails c`

to run the test suite `bundle exec rake test`

