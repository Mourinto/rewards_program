class RewardsController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :validate_attached_file

  def calculate_rewards
    events = Rewards::DataValidation.new(params[:file]).extract_valid_events
    users_chains = Rewards::ReferralExtractor.new(events).build_referral_chains
    results = Rewards::PointsCalculations.new(users_chains).calculate_points
    render json: Rewards::ResponseMessage.success(results)
  rescue StandardError => e
    render json: Rewards::ResponseMessage.error(e.message)
  end

  private

  def validate_attached_file
    if params[:file].blank?
      render json: Rewards::ResponseMessage.error('Please Attach Valid File')
    elsif params[:file].content_type != 'text/plain'
      render json: Rewards::ResponseMessage.error('Invalid File Type, Please attach text file!')
    end
  end
end

