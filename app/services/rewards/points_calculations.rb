module Rewards
  class PointsCalculations

    def initialize(mapped_data)
      @users_chains = mapped_data
    end

    def calculate_points
      @users_chains.keys.each do |user_id|
        points_factor = 1
        calculate_chain_points(user_id, points_factor)
      end
      @users_chains
    end

    def calculate_chain_points(user_id, points_factor)
      user = @users_chains[user_id]
      referrer = @users_chains[user.referrer_id]
      return if referrer.blank?
      referrer.points += points_factor
      points_factor *= 0.5
      calculate_chain_points(user.referrer_id, points_factor)
    end

    # another approach to calculate points through database
    #
    # def self.perform(referrer_id)
    #   points_factor = 1
    #   chain_points(referrer_id).each do |selection|
    #     ReferralChain.find_by(user_id: selection['user_id'])
    #         .update(points: selection['points'] + points_factor)
    #     points_factor *= 0.5
    #   end
    # end
    #
    # def self.chain_points(referrer_id)
    #   tree_query =
    #       <<-SQL.squish
    #           WITH RECURSIVE referral_tree AS (
    #           SELECT * FROM referral_chains WHERE referral_chains.user_id = "#{referrer_id}"
    #           UNION ALL
    #           SELECT
    #           referral_chains.*
    #           FROM referral_chains
    #           JOIN referral_tree ON referral_tree.referrer_id = referral_chains.user_id
    #           )
    #           SELECT DISTINCT user_id, points FROM referral_tree
    #           ORDER BY user_id DESC;
    #   SQL
    #   ActiveRecord::Base.connection.execute(tree_query).to_a
    # end

  end
end