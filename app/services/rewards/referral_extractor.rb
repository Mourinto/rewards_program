module Rewards
  class ReferralExtractor

    def initialize(events)
      @events = events
      @users_chains = {}
    end

    def build_referral_chains
      @events.each { |event| create_or_accept_referral(event) }
      @users_chains
    end

    private

    def create_or_accept_referral(event)
      if event[:recommends_event]
        create_new_user(event)
      elsif event[:accepts_event] && user_exists?(event[:event_doer])
        mark_invitation_accepted(event[:event_doer])
      end
    end

    def create_new_user(event)
      create_root_user(event) if @users_chains.blank? || !user_exists?(event[:event_doer])
      return if user_exists?(event[:event_target]) #user was invited before
      create_referred_user(event)
    end

    def create_root_user(event)
      create_user(event[:event_doer], event[:event_time])
    end

    def create_referred_user(event)
      create_user(event[:event_target], event[:event_time], event[:event_doer])
    end

    def create_user(user_id, event_time, referrer_id = nil)
      user = Rewards::UserReferringConstructor.new(user_id, event_time, referrer_id)
      @users_chains[user.user_id] = user
    end

    def user_exists?(selected_user)
      @users_chains[selected_user].present?
    end

    def mark_invitation_accepted(event_doer)
      user = @users_chains[event_doer]
      user.invite_accepted = true
      @users_chains[event_doer] = user
    end
  end
end