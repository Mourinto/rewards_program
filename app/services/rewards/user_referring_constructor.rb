module Rewards
  class UserReferringConstructor
    attr_accessor :invite_accepted, :user_id, :referrer_id, :points

    def initialize(user_id, invitation_date, referrer_id = nil)
      @user_id = user_id
      @invite_accepted = referrer_id.blank?
      @referrer_id = referrer_id
      @invitation_date = invitation_date
      @points = 0.0
    end

  end
end