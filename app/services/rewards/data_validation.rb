module Rewards
  class DataValidation

    def initialize(file)
      @data_file = file.read
    end

    def extract_valid_events
      raise Rewards::Errors::InvalidFileExceptions.new('File is Empty!, please provide a valid file.') if @data_file.strip.blank?
      extracted_data = @data_file.split(/\n/).map { |line| line.split(" ") }
      validate_extracted_data(extracted_data)
      extracted_data.map { |extracted_event| build_event_hash(extracted_event) }.sort_by { |hash| hash[:event_time] }
    end

    private

    def validate_extracted_data(extracted_data)
      extracted_data.map do |line|
        unless valid_action(line) && valid_referring_data(line)
          raise Rewards::Errors::InvalidFileExceptions.new('Data is corrupted!, please provide valid data.')
        end
      end
    end

    def valid_action(line)
      line.include?('recommends') || line.include?('accepts')
    end

    def valid_referring_data(line)
      (line.include?('recommends') && line.length == 5) || (line.include?('accepts') && line.length == 4)
    end

    def build_event_hash(extracted_event)
      {
          event_time: extracted_event[0..1].join(" ").to_datetime,
          event_doer: extracted_event[2],
          event_target: extracted_event[4],
          recommends_event: extracted_event.include?('recommends'),
          accepts_event: extracted_event.include?('accepts')
      }
    end
  end
end