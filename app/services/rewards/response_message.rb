module Rewards
  class ResponseMessage

    def self.success(users_chains)
      response = {}
      users_chains.each { |user_id, user| response[user_id] = user.points }
      response
    end

    def self.error(message)
      { error: message }
    end
  end
end